using System.Collections.Generic;
using MongoDB.Driver;

public class UserGroupRepository : IUserGroupRepository
{
    private readonly IMongoCollection<UserGroupModel> __UserGroups;

    public UserGroupRepository(IAuthenticationDatabaseSettings settings)
    {
        var client = new MongoClient(settings.ConnectionString);
        var database = client.GetDatabase(settings.DatabaseName);

        __UserGroups = database.GetCollection<UserGroupModel>(settings.UserGroupCollectionName);
    }

    public List<UserGroupModel> Get() =>
        __UserGroups.Find(group => true).ToList();

    public UserGroupModel Get(string id) =>
    __UserGroups.Find<UserGroupModel>(group => group.Id == id).FirstOrDefault();

    public UserGroupModel Create(UserGroupModel group)
    {
        __UserGroups.InsertOne(group);
        return group;
    }

    public void Update(string id, UserGroupModel groupIn) =>
        __UserGroups.ReplaceOne(group => group.Id == id, groupIn);

    public void Remove(UserGroupModel groupIn) =>
        __UserGroups.DeleteOne(group => group.Id == groupIn.Id);

    public void Remove(string id) =>
        __UserGroups.DeleteOne(group => group.Id == id);
}