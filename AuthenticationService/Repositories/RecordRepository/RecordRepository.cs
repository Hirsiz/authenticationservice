using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using MongoDB.Driver;

public class RecordRepository : IRecordRepository
{
    private readonly IMongoCollection<Record> __Records;

    public RecordRepository(IAuthenticationDatabaseSettings settings)
    {
        MongoClient _Client = new MongoClient(settings.ConnectionString);
        IMongoDatabase _Database = _Client.GetDatabase(settings.DatabaseName);

        __Records = _Database.GetCollection<Record>(settings.RecordCollectionName);
    }

    public List<Record> Get() =>
            __Records.Find(record => true).ToList();

    public Record Get(string id) =>
            __Records.Find<Record>(record => record.ID == id).FirstOrDefault();

    public Record Create(Record record)
    {
        __Records.InsertOne(record);
        return record;
    }

    public void Update(string id, Record recordIn) =>
        __Records.ReplaceOne(record => record.ID == id, recordIn);

}