using System.Collections.Generic;
using FamilyHistoryService.Models;

public interface IRecordRepository
{
    List<Record> Get();
    Record Get(string id);
    Record Create(Record record);
    void Update(string id, Record recordIn);
}