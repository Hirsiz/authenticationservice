using System.Collections.Generic;
using Models.UserEntity;
using MongoDB.Driver;

public class AuthenticationRepository : IAuthenticationRepository
{
    private readonly IMongoCollection<UserEntity> __Users;

    public AuthenticationRepository(IAuthenticationDatabaseSettings settings)
    {
        var client = new MongoClient(settings.ConnectionString);
        var database = client.GetDatabase(settings.DatabaseName);

        __Users = database.GetCollection<UserEntity>(settings.AuthenticationCollectionName);
    }

    public List<UserEntity> Get() =>
        __Users.Find(user => true).ToList();

    public UserEntity Get(string id) =>
        __Users.Find<UserEntity>(user => user.Id == id).FirstOrDefault();

    public UserEntity Create(UserEntity user)
    {
        __Users.InsertOne(user);
        return user;
    }

    public void Update(string id, UserEntity bookIn) =>
        __Users.ReplaceOne(user => user.Id == id, bookIn);

    public void Remove(UserEntity bookIn) =>
        __Users.DeleteOne(user => user.Id == bookIn.Id);

    public void Remove(string id) =>
        __Users.DeleteOne(user => user.Id == id);
}