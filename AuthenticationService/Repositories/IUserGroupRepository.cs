using System.Collections.Generic;

public interface IUserGroupRepository
{
    List<UserGroupModel> Get();
    UserGroupModel Get(string id);
    UserGroupModel Create(UserGroupModel user);
    void Update(string id, UserGroupModel user);
    void Remove(UserGroupModel user);
    void Remove(string id);
}