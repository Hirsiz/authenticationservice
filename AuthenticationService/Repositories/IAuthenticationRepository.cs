using System.Collections.Generic;
using Models.UserEntity;

public interface IAuthenticationRepository
{
    List<UserEntity> Get();
    UserEntity Get(string id);
    UserEntity Create(UserEntity user);
    void Update(string id, UserEntity user);
    void Remove(UserEntity user);
    void Remove(string id);
}