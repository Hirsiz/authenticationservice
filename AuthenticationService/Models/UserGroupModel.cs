using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Models.UserEntity;
using System;

public class UserGroupModel
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string Name { get; set; }
    public List<UserEntity> Users { get; set; }
    public Guid AccessToken { get; set; }
}
