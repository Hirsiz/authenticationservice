public class CurrentUserRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
}