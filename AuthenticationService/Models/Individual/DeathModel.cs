using System;

namespace FamilyHistoryService.Models.Individual
{
    public class DeathModel
    {
        public string Place { get; set; }
        public string Date { get; set; }
    }
}