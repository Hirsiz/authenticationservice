using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using FamilyHistoryService.Models.Family;
using FamilyHistoryService.Models.Individual;
using System.Collections.Generic;
using System;

namespace FamilyHistoryService.Models
{
    public class Record
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string ID { get; set; }
        public List<FamilyModel> Family { get; set; }
        public IndividualModel Individual { get; set; }
        public string FileID { get; set; }
        public InformationModel Information { get; set; }
        public Guid AccessToken { get; set; }
        public string MaintainerGroupID { get; set; }
        public string VisibilityGroupID { get; set; }
    }
}