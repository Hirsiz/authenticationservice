public interface IAuthenticationDatabaseSettings
{
    string AuthenticationCollectionName { get; set; }
    string UserGroupCollectionName { get; set; }
    string RecordCollectionName { get; set; }
    string FilesCollectionName { get; set; }
    string ConnectionString { get; set; }
    string DatabaseName { get; set; }
}