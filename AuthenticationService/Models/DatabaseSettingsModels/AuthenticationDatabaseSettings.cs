public class AuthenticationDatabaseSettings : IAuthenticationDatabaseSettings
{
    public string AuthenticationCollectionName { get; set; }
    public string UserGroupCollectionName { get; set; }
    public string RecordCollectionName { get; set; }
    public string FilesCollectionName { get; set; }
    public string ConnectionString { get; set; }
    public string DatabaseName { get; set; }
}