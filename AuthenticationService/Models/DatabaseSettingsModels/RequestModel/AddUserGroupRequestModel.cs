using System;

public class AddUserGroupRequestModel
{
    public string Name { get; set; }
    public Guid AccessToken { get; set; }
}