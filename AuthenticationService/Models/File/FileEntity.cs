using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public class FileEntity
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string ID { get; set; }
    public string Name { get; set; }
    public string TreeName { get; set; }
    public string Extension { get; set; }
    public string MaintainerGroup { get; set; }
    public string VisibilityGroup { get; set; }
    public Guid AccessToken { get; set; }
}