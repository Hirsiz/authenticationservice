using System;

namespace FamilyHistoryService.Models.Family
{
    public class MarriageModel
    {
        public string Place { get; set; }
        public DateTime Date { get; set; }

    }
}