using System;
using System.Collections.Generic;
using System.Linq;
using Models.UserEntity;

public class AuthenticationManager : IAuthenticationManager
{
    public IAuthenticationRepository __AuthenticationRepository;

    public AuthenticationManager(IAuthenticationRepository authenticationRepository)
    {
        __AuthenticationRepository = authenticationRepository;
    }

    public bool CheckUserExists(string username)
    {
        List<UserEntity> _Users = __AuthenticationRepository.Get();
        List<UserEntity> _User = _Users.Where(user => user.Username == username).ToList();

        return _Users.Any(user => user.Username == username);
    }

    public UserEntity CreateUser(NewUserRequestModel requestModel)
    {
        //check if username doesnt exist first
        List<UserEntity> _Users = __AuthenticationRepository.Get();
        List<UserEntity> _User = _Users.Where(user => user.Username == requestModel.Username).ToList();

        //create a new user 
        if (_User.Count == 0)
        {
            UserEntity _NewUser = new UserEntity()
            {
                Firstname = requestModel.Firstname,
                Lastname = requestModel.Lastname,
                Username = requestModel.Username,
                Password = requestModel.Password,
                AccessToken = Guid.NewGuid()
            };
            return __AuthenticationRepository.Create(_NewUser);
        }
        return null;

    }

    public UserEntity GetUser(string username, string password)
    {
        List<UserEntity> _Users = __AuthenticationRepository.Get();
        UserEntity _User = _Users.Where(user => user.Username.ToLower() == username.ToLower() && user.Password == password).SingleOrDefault();

        return _User;
    }

    public List<UserModel> GetUsers()
    {
        List<UserEntity> _Users = __AuthenticationRepository.Get();
        List<UserModel> _UserModels = new List<UserModel>();
        foreach (UserEntity user in _Users)
        {
            _UserModels.Add(new UserModel()
            {
                ID = user.Id,
                Username = user.Username,
                Firstname = user.Firstname,
                Lastname = user.Lastname
            });
        }
        return _UserModels;
    }
}