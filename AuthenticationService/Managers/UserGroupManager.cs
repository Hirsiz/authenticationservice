using System;
using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using Models.UserEntity;

public class UserGroupManager : IUserGroupManager
{
    private IUserGroupRepository __UserGroupRepository;
    private IAuthenticationRepository __AuthenticationRepository;
    private IRecordRepository __RecordRepository;
    private IFileRepository __FileRepository;

    public UserGroupManager(IUserGroupRepository userGroupRepository, IAuthenticationRepository authenticationRepository, IRecordRepository recordRepository, IFileRepository fileRepository)
    {
        __UserGroupRepository = userGroupRepository;
        __AuthenticationRepository = authenticationRepository;
        __RecordRepository = recordRepository;
        __FileRepository = fileRepository;
    }

    public List<UserGroupModel> GetUserGroups(Guid accessToken)
    {
        return __UserGroupRepository.Get().Where(group => group.AccessToken == accessToken).ToList();
    }

    public UserGroupModel GetUserGroup(string id)
    {
        return __UserGroupRepository.Get(id);
    }

    public List<UserGroupModel> DeleteUserGroup(string id)
    {
        __UserGroupRepository.Remove(id);
        //get every tree with this user group and set the group to "0"
        List<Record> _Records = __RecordRepository.Get();
        List<FileEntity> _Files = __FileRepository.Get();
        foreach (FileEntity file in _Files)
        {
            if (file.MaintainerGroup == id)
            {
                file.MaintainerGroup = "0";
            }

            if (file.VisibilityGroup == id)
            {
                file.VisibilityGroup = "0";
            }

            __FileRepository.Update(file.ID, file);
        }

        foreach (Record record in _Records)
        {
            if (record.MaintainerGroupID == id)
            {
                record.MaintainerGroupID = "0";
            }

            if (record.VisibilityGroupID == id)
            {
                record.VisibilityGroupID = "0";
            }
            __RecordRepository.Update(record.ID, record);
        }

        return __UserGroupRepository.Get();
    }

    public List<UserGroupModel> AddUserGroup(AddUserGroupRequestModel userGroupRequest)
    {
        UserGroupModel _UserGroupModel = new UserGroupModel()
        {
            Name = userGroupRequest.Name,
            Users = new List<UserEntity>(),
            AccessToken = userGroupRequest.AccessToken
        };
        __UserGroupRepository.Create(_UserGroupModel);
        return __UserGroupRepository.Get();
    }

    public List<UserGroupModel> DeleteUserFromGroup(string userGroupID, string userID)
    {
        UserGroupModel _UserGroup = __UserGroupRepository.Get(userGroupID);
        // List<UserEntity> _Users = _UserGroup.Users;

        _UserGroup.Users = _UserGroup.Users.Where(user => user.Id != userID).ToList();

        __UserGroupRepository.Update(userGroupID, _UserGroup);
        return __UserGroupRepository.Get();
    }

    public List<UserGroupModel> AddUserToGroup(string userGroupID, string userID)
    {
        UserGroupModel _UserGroup = __UserGroupRepository.Get(userGroupID);
        List<UserEntity> _Users = _UserGroup.Users;

        UserEntity _NewUser = __AuthenticationRepository.Get(userID);
        if (!_Users.Any(user => user.Id == _NewUser.Id))
        {
            _Users.Add(_NewUser);
        }

        __UserGroupRepository.Update(userGroupID, _UserGroup);
        return __UserGroupRepository.Get();
    }
}