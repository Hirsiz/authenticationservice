using System.Collections.Generic;
using Models.UserEntity;

public interface IAuthenticationManager
{
    UserEntity CreateUser(NewUserRequestModel requestModel);
    UserEntity GetUser(string username, string password);
    List<UserModel> GetUsers();
    bool CheckUserExists(string username);
}