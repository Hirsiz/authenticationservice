using System;
using System.Collections.Generic;

public interface IUserGroupManager
{
    List<UserGroupModel> GetUserGroups(Guid accessToken);
    UserGroupModel GetUserGroup(string id);
    List<UserGroupModel> DeleteUserGroup(string id);
    List<UserGroupModel> AddUserGroup(AddUserGroupRequestModel userGroup);
    List<UserGroupModel> DeleteUserFromGroup(string userGroupID, string userID);
    List<UserGroupModel> AddUserToGroup(string userGroupID, string userID);
}