using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace AuthenticationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserGroupController : ControllerBase
    {
        private IAuthenticationManager __AuthenticationManager;
        private IUserGroupManager __UserGroupManager;

        public UserGroupController(IAuthenticationManager authenticationManager, IUserGroupManager userGroupManager)
        {
            __AuthenticationManager = authenticationManager;
            __UserGroupManager = userGroupManager;
        }

        [HttpGet]
        [Route("GetUserGroups")]
        public ActionResult<List<UserGroupModel>> GetUserGroups(Guid accessToken)
        {
            return __UserGroupManager.GetUserGroups(accessToken);
        }

        [HttpGet]
        [Route("GetUserGroup")]
        public ActionResult<UserGroupModel> GetUserGroup(string id)
        {
            return __UserGroupManager.GetUserGroup(id);
        }

        [HttpDelete]
        [Route("DeleteUserGroup")]
        public ActionResult<List<UserGroupModel>> DeleteUserGroup([FromBody]string id)
        {
            return __UserGroupManager.DeleteUserGroup(id);
        }

        [HttpPost]
        [Route("AddUserGroup")]
        public ActionResult<List<UserGroupModel>> AddUserGroup(AddUserGroupRequestModel userGroup)
        {
            return __UserGroupManager.AddUserGroup(userGroup);
        }

        [HttpPost]
        [Route("AddUserToGroup")]
        public ActionResult<List<UserGroupModel>> AddUserToGroup(UserRequestModel request)
        {
            return __UserGroupManager.AddUserToGroup(request.UserGroupID, request.UserID);
        }

        [HttpDelete]
        [Route("DeleteUserFromGroup")]
        public ActionResult<List<UserGroupModel>> DeleteUserFromGroup(UserRequestModel request)
        {
           return __UserGroupManager.DeleteUserFromGroup(request.UserGroupID, request.UserID);
        }

    }
}