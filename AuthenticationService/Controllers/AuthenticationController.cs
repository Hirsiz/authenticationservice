﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.UserEntity;

namespace AuthenticationService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private IAuthenticationManager __AuthenticationManager;
        private IUserGroupManager __UserGroupManager;

        public AuthenticationController(IAuthenticationManager authenticationManager, IUserGroupManager userGroupManager)
        {
            __AuthenticationManager = authenticationManager;
            __UserGroupManager = userGroupManager;
        }

        [HttpGet]
        public ActionResult<UserEntity> Get(string username, string password)
        {
            return __AuthenticationManager.GetUser(username, password);
        }

        [HttpGet]
        [Route("GetAllUsers")]
        public ActionResult<List<UserModel>> GetAllUsers()
        {
            return __AuthenticationManager.GetUsers();
        }

        [HttpPost]
        [Route("CreateUser")]
        public ActionResult<UserEntity> CreateUser(NewUserRequestModel requestModel)
        {
            // return null;
            return __AuthenticationManager.CreateUser(requestModel);
        }

        [HttpGet]
        [Route("CheckUserExists")]
        public ActionResult<bool> CheckUserExists(string username){
            return __AuthenticationManager.CheckUserExists(username);
        }
    }
}
